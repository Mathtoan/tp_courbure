

#include <GL/glew.h>

#include "scene.hpp"

#include "../../lib/opengl/glutils.hpp"
#include "../../lib/3d/mat2.hpp"
#include "../../lib/3d/vec3.hpp"
#include "../../lib/perlin/perlin.hpp"
#include "../../lib/interface/camera_matrices.hpp"

#include "../interface/myWidgetGL.hpp"

#include <cmath>

#include <string>
#include <sstream>
#include "../../lib/mesh/mesh_io.hpp"


using namespace cpe;




void scene::load_scene(int curv, int surf) {

    c = curv;
    s = surf;

    load_common_data();

    build_surface();

    surface.fill_normal();
    surface.fill_empty_field_by_default();

    surface_opengl.fill_vbo(surface);

}




void scene::draw_scene() {
    //Setup uniform parameters
    glUseProgram(shader_program_id);                                                                           PRINT_OPENGL_ERROR();

    //Get cameras parameters (modelview,projection,normal).
    camera_matrices const& cam=pwidget->camera();

    //Set Uniform data to GPU
    glUniformMatrix4fv(get_uni_loc(shader_program_id,"camera_modelview"), 1,false,cam.modelview.pointer());    PRINT_OPENGL_ERROR();
    glUniformMatrix4fv(get_uni_loc(shader_program_id,"camera_projection"),1,false,cam.projection.pointer());   PRINT_OPENGL_ERROR();
    glUniformMatrix4fv(get_uni_loc(shader_program_id,"normal_matrix"),    1,false,cam.normal.pointer());       PRINT_OPENGL_ERROR();

    glBindTexture(GL_TEXTURE_2D,texture_default);                                                              PRINT_OPENGL_ERROR();
    
    surface_opengl.draw();

}


void scene::reset() {
    surface = cpe::mesh_parametric();
    surface_opengl = cpe::mesh_opengl();
}





scene::scene() : shader_program_id(0), s(0), c(0) {}


GLuint scene::load_texture_file(std::string const& filename) {
    return pwidget->load_texture_file(filename);
}


void scene::set_widget(myWidgetGL* widget_param) {
    pwidget=widget_param;
}


void scene::load_common_data() {
    texture_default = load_texture_file("data/white.jpg");
    shader_program_id = read_shader("shaders/shader_mesh.vert",
                                    "shaders/shader_mesh.frag"); PRINT_OPENGL_ERROR();
}




cpe::vec3 scene::colormap_cool_warm(double k) {
    // "Diverging Color Maps for Scientific Visualization" by Kenneth Moreland (Sandia National Laboratories)
    // https://www.kennethmoreland.com/color-maps/ColorMapsExpanded.pdf

    if (k < 0.0) {k = 0.0;}
    if (k > 1.0) {k = 1.0;}

    vec3 colormap[] {
        {59, 76, 192},
        {68, 90, 204},
        {77, 104, 215},
        {87, 117, 225},
        {98, 130, 234},
        {108, 142, 241},
        {119, 154, 247},
        {130, 165, 251},
        {141, 176, 254},
        {152, 185, 255},
        {163, 194, 255},
        {174, 201, 253},
        {184, 208, 249},
        {194, 213, 244},
        {204, 217, 238},
        {213, 219, 230},
        {221, 221, 221},
        {229, 216, 209},
        {236, 211, 197},
        {241, 204, 185},
        {245, 196, 173},
        {247, 187, 160},
        {247, 177, 148},
        {247, 166, 135},
        {244, 154, 123},
        {241, 141, 111},
        {236, 127, 99},
        {229, 112, 88},
        {222, 96, 77},
        {213, 80, 66},
        {203, 62, 56},
        {192, 40, 47},
        {180, 4, 38}};

    int i = int(k*32);

    return colormap[i];
}










void scene::build_surface() {
    int const Nu=300;
    int const Nv=300;
    
    surface.set_plane_xy_unit(Nu,Nv);

    switch(s) {
        case 0:
            create_surface_Sphere(Nu,Nv);
            break;
        case 1:
            create_surface_Hyperbolic_Paraboloid(Nu,Nv);
            break;
        case 2:
            create_surface_Catenoid (Nu,Nv);
            break;
        case 3:
            create_surface_Right_Helicoid (Nu,Nv);
            break;
        case 4:
            create_surface_Pseudo_Sphere (Nu,Nv);
            break;
        case 5:
            create_surface_Sinusoid (Nu,Nv);
            break;
        case 6:
            create_surface_Gaussian (Nu,Nv);
            break;

        default:
            create_surface_Hyperbolic_Paraboloid(Nu,Nv);
    }

}







void scene::create_surface_Sphere (int Nu, int Nv) {
    double const u_min = -M_PI;
    double const u_max = M_PI;
    double const v_min = 0;
    double const v_max = 2.0*M_PI;
    
    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double const r = 0.2f;

    double k;
    double K[Nu*Nv];

    for(int ku=0 ; ku<Nu ; ++ku) {
        for(int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = r*cos(u)*cos(v);
            double const y = r*sin(u)*cos(v);
            double const z = r*sin(v);

            surface.vertex(ku,kv) = {x,y,z};

            if (c == 0) {
                K[ku*Nu + kv] = 1/(r*r);

            } if (c == 1) {
                K[ku*Nu + kv] = 1/r;
            }
            
        }
    }


    if (c == 0 || c == 1) {

        double MIN = *std::min_element(K, K + Nu*Nv);
        double MAX = *std::max_element(K, K + Nu*Nv);

        double max_abs = std::max(std::abs(MIN), std::abs(MAX));

        for (int ku=0 ; ku<Nu ; ++ku) {
            for (int kv=0 ; kv<Nv ; ++kv) {
                k = K[ku*Nu + kv] / max_abs;
                k = (k+1)/2;
                surface.color(ku,kv) = colormap_cool_warm(k)/255;
            }
        }
    } else {
        set_color(Nu, Nv, du, dv);
    }
    
}




void scene::create_surface_Hyperbolic_Paraboloid (int Nu, int Nv) {
    double const u_min = -1;
    double const u_max = 1;
    double const v_min = -1;
    double const v_max = 1;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double const a = 0.5f;
    double const b = 0.5f;
    double const h = 0.5f;
    double q;

    double k;
    double K[Nu*Nv];

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = a*u;
            double const y = b*v;
            double const z = h*(u*u-v*v);

            surface.vertex(ku,kv) = {x,y,z};

            if (c == 0) {
                q = (a*a)/(2*h);
                k = - (q*q) / std::pow(q*q + x*x + y*y, 2.0);
                K[ku*Nu + kv] = k;

            } if (c == 1) {
                q = (a*a)/(2*h);
                k = - (x*y) / std::pow(q*q + x*x + y*y, 3.0/2.0);
                K[ku*Nu + kv] = k;
            }
            
        }

    }

    
    if (c == 0 || c == 1) {

        double MIN = *std::min_element(K, K + Nu*Nv);
        double MAX = *std::max_element(K, K + Nu*Nv);

        double max_abs = std::max(std::abs(MIN), std::abs(MAX));

        for (int ku=0 ; ku<Nu ; ++ku) {
            for (int kv=0 ; kv<Nv ; ++kv) {
                k = K[ku*Nu + kv] / max_abs;
                k = (k+1)/2;
                surface.color(ku,kv) = colormap_cool_warm(k)/255;
            }
        }

    } else {
        set_color(Nu, Nv, du, dv);
    }

}





void scene::create_surface_Catenoid (int Nu, int Nv) {
    double const u_min = -1.8;//-M_PI;
    double const u_max = 1.8;//M_PI;
    double const v_min = 0.0f;
    double const v_max = 2.0f*M_PI;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double const a = 0.1f;
    double const b = 0.1f;
    double const d = 0.1f;

    double k;
    double K[Nu*Nv];


    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = a*cosh(u)*cos(v);
            double const y = b*cosh(u)*sin(v);
            double const z = d*u;

            surface.vertex(ku,kv) = {x,y,z};


            if (c == 0) {
                k = - 1 / (a*a * std::pow(std::cosh(z/a), 4.0));
                K[ku*Nu + kv] = k;

            } if (c == 1) {
                K[ku*Nu + kv] = 0;
            }

        }
    }


    if (c == 0 || c == 1) {

        double MIN = *std::min_element(K, K + Nu*Nv);
        double MAX = *std::max_element(K, K + Nu*Nv);

        double max_abs;

        if (MIN == 0.0 && MAX == 0.0)  
            max_abs = 1.0; 
        else
            max_abs = std::max(std::abs(MIN), std::abs(MAX));
        

        for (int ku=0 ; ku<Nu ; ++ku) {
            for (int kv=0 ; kv<Nv ; ++kv) {
                k = K[ku*Nu + kv] / max_abs;
                k = (k+1)/2;
                surface.color(ku,kv) = colormap_cool_warm(k)/255;
            }
        }

    } else {
        set_color(Nu, Nv, du, dv);
    }
}




void scene::create_surface_Right_Helicoid (int Nu, int Nv) {
    double const u_min = -M_PI;
    double const u_max = M_PI;
    double const v_min = -M_PI;
    double const v_max = M_PI;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double const a = 0.1f;
    double const b = 0.1f;
    double const h = 0.1f;

    double k;
    double K[Nu*Nv];

    for(int ku=0 ; ku<Nu ; ++ku) {
        for(int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = a*u*cos(v);
            double const y = b*u*sin(v);
            double const z = h*v;

            surface.vertex(ku,kv) = {x,y,z};

            if (c == 0) {
                k = - (h*h) / (h*h + a*u*u);
                K[ku*Nu + kv] = k;

            } if (c == 1) {
                K[ku*Nu + kv] = 0;
            }
        }
    }
    
    if (c == 0 || c == 1) {

        double MIN = *std::min_element(K, K + Nu*Nv);
        double MAX = *std::max_element(K, K + Nu*Nv);

        double max_abs;

        if (MIN == 0.0 && MAX == 0.0)  
            max_abs = 1.0; 
        else
            max_abs = std::max(std::abs(MIN), std::abs(MAX));
        

        for (int ku=0 ; ku<Nu ; ++ku) {
            for (int kv=0 ; kv<Nv ; ++kv) {
                k = K[ku*Nu + kv] / max_abs;
                k = (k+1)/2;
                surface.color(ku,kv) = colormap_cool_warm(k)/255;
            }
        }

    } else {
        set_color(Nu, Nv, du, dv);
    }
}



void scene::create_surface_Pseudo_Sphere (int Nu, int Nv) {
    double const u_min = -M_PI;
    double const u_max = M_PI;
    double const v_min = 0.0f;
    double const v_max = 2.0f*M_PI;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double const r = 0.2f;

    double k;
    double K[Nu*Nv];

    for(int ku=0 ; ku<Nu ; ++ku) {
        for(int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = r*cos(v)/cosh(u);
            double const y = r*sin(v)/cosh(u);
            double const z = r*(u-tanh(u));

            surface.vertex(ku,kv) = {x,y,z};

            if (c == 0) {
                K[ku*Nu + kv] = -1/(r*r);

            } if (c == 1) {
                K[ku*Nu + kv] = -1/r;
            }

        }
    }

    if (c == 0 || c == 1) {

        double MIN = *std::min_element(K, K + Nu*Nv);
        double MAX = *std::max_element(K, K + Nu*Nv);

        double max_abs = std::max(std::abs(MIN), std::abs(MAX));

        for (int ku=0 ; ku<Nu ; ++ku) {
            for (int kv=0 ; kv<Nv ; ++kv) {
                k = K[ku*Nu + kv] / max_abs;
                k = (k+1)/2;
                surface.color(ku,kv) = colormap_cool_warm(k)/255;
            }
        }

    } else {
        set_color(Nu, Nv, du, dv);
    }
    

}




void scene::create_surface_Sinusoid (int Nu, int Nv) {
    double const u_min = -1;
    double const u_max = 1;
    double const v_min = -1;
    double const v_max = 1;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double a = 10;
    double const f_x = 2;
    double const f_y = 1;

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = u;
            double const y = v;
            double const z = (sin(2*M_PI*f_x*(u))+sin(2*M_PI*f_y*(v)))/a;

            surface.vertex(ku,kv) = {x,y,z};
        }
    }
    set_color(Nu, Nv, du, dv);


}




void scene::create_surface_Gaussian(int Nu, int Nv) {
    double const u_min = -1;
    double const u_max = 1;
    double const v_min = -1;
    double const v_max = 1;

    double const du = (u_max-u_min)/Nu;
    double const dv = (v_max-v_min)/Nv;

    double a = 0.5f;
    double const sigma_x = 0.1;
    double const sigma_y = 0.1;

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            double const u_n = static_cast<double>(ku)/(Nu-1);
            double const v_n = static_cast<double>(kv)/(Nv-1);

            double const u = u_min + u_n * (u_max-u_min);
            double const v = v_min + v_n * (v_max-v_min);

            double const x = u;
            double const y = v;
            double const z = a*exp(-(u*u)/(2*sigma_x*sigma_x)-(v*v)/(2*sigma_y*sigma_y));

            surface.vertex(ku,kv) = {x,y,z};
        }
    }
    set_color(Nu, Nv, du, dv);


}











vec2 scene::principal_curvatures(vec3 const& Su, vec3 const& Sv, vec3 const& Suu, vec3 const& Svv, vec3 const& Suv) {
    vec3 n = normalized(cross(Su, Sv));

    mat2 Is = mat2(norm(Su)*norm(Su), dot(Su, Sv),
                   dot(Su, Sv),       norm(Sv)*norm(Sv));

    mat2 IIs = -1.0*mat2(dot(n, Suu), dot(n, Suv),
                         dot(n, Suv), dot(n, Svv));

    mat2 Ws = IIs*inverse(Is);

    return eigenvalue(Ws);
}




void scene::set_color(int Nu, int Nv, double du, double dv) {

    vec3 *Su = derivate_u(Nu, Nv, du);
    vec3 *Sv = derivate_v(Nu, Nv, dv);
    vec3 *Suu = derivate2_u(Nu, Nv, du);
    vec3 *Svv = derivate2_v(Nu, Nv, dv);
    vec3 *Suv = derivate2_uv(Nu, Nv, du, dv);

    

    double k;
    double K[Nu*Nv];

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            vec2 lambda = principal_curvatures(Su[ku*Nu + kv], Sv[ku*Nu + kv], Suu[ku*Nu + kv], Svv[ku*Nu + kv], Suv[ku*Nu + kv]);
            
            switch(c) {
                case 2:
                    K[ku*Nu + kv] = lambda(0)*lambda(1);
                    break;
                case 3:
                    K[ku*Nu + kv] = 0.5*(lambda(0)+lambda(1));
                    break;
                case 4:
                    K[ku*Nu + kv] = lambda(0);
                    break;
                case 5:
                    K[ku*Nu + kv] = lambda(1);
                    break;

                default:
                    K[ku*Nu + kv] = lambda(0)*lambda(1);
            }
        }
    }


    double MIN = *std::min_element(K, K + Nu*Nv);
    double MAX = *std::max_element(K, K + Nu*Nv);

    double max_abs = std::max(std::abs(MIN), std::abs(MAX));

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            k = K[ku*Nu + kv] / max_abs;
            k = (k+1)/2;
            surface.color(ku,kv) = colormap_cool_warm(k)/255;
        }
    }

}









cpe::vec3* scene::derivate_u(int Nu, int Nv, double du) {
    vec3 *ret = new vec3[Nu*Nv];

    for (int ku=1 ; ku<Nu-1 ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            vec3 el = (surface.vertex(ku+1, kv)-surface.vertex(ku-1, kv))/du;
            ret[ku*Nu + kv] = el;
        }
    }
    for (int kv=0 ; kv<Nv ; ++kv){
        ret[kv] = ret[Nu + kv];
        ret[(Nu-1)*Nu + kv] = ret[(Nu-2)*Nu + kv];
    }

    return ret;
}


cpe::vec3* scene::derivate_v(int Nu, int Nv, double dv) {
    vec3 *ret = new vec3[Nu*Nv];

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=1 ; kv<Nv-1 ; ++kv) {
            vec3 el = (surface.vertex(ku,kv+1)-surface.vertex(ku,kv-1))/dv;
            ret[ku*Nu + kv] = el;
        }
    }
    for (int ku=0 ; ku<Nu ; ++ku){
        ret[ku*Nu] = ret[ku*Nu + 1];
        ret[ku*Nu + Nv-1] = ret[ku*Nu + Nv - 2];
    }

    return ret;
}


cpe::vec3* scene::derivate2_u(int Nu, int Nv, double du) {
    vec3 *ret = new vec3[Nu*Nv];

    for (int ku=1 ; ku<Nu-1 ; ++ku) {
        for (int kv=0 ; kv<Nv ; ++kv) {
            vec3 el = (surface.vertex(ku+1, kv)-2*surface.vertex(ku,kv)+surface.vertex(ku-1,kv))/(du*du);
            ret[ku*Nu + kv] = el;
        }
    }
    for (int kv=0 ; kv<Nv ; ++kv){
        ret[kv] = ret[Nu + kv];
        ret[(Nu-1)*Nu + kv] = ret[(Nu-2)*Nu + kv];
    }

    return ret;
}



cpe::vec3* scene::derivate2_v(int Nu, int Nv, double dv) {
    vec3 *ret = new vec3[Nu*Nv];

    for (int ku=0 ; ku<Nu ; ++ku) {
        for (int kv=1 ; kv<Nv-1 ; ++kv) {
            vec3 el = (surface.vertex(ku, kv+1)-2*surface.vertex(ku,kv)+surface.vertex(ku,kv-1))/(dv*dv);
            ret[ku*Nu + kv] = el;
        }
    }
    for (int ku=0 ; ku<Nu ; ++ku){
        ret[ku*Nu] = ret[ku*Nu + 1];
        ret[ku*Nu + Nv-1] = ret[ku*Nu + Nv - 2];
    }

    return ret;
}



cpe::vec3* scene::derivate2_uv(int Nu, int Nv, double du, double dv) {
    vec3 *ret = new vec3[Nu*Nv];

    for (int ku=1 ; ku<Nu-1 ; ++ku) {
        for (int kv=1 ; kv<Nv-1 ; ++kv) {
            vec3 el = surface.vertex(ku+1,kv+1)-surface.vertex(ku+1, kv-1)-surface.vertex(ku-1,kv+1)+surface.vertex(ku-1,kv-1);
            el = el/(4*du*dv);
            ret[ku*Nu + kv] = el;
        }
    }
    
    for (int kv=1 ; kv<Nv-1 ; ++kv) {
        ret[kv*Nu] = ret[kv*Nu + 1];
        ret[kv*Nu + Nv-1] = ret[kv*Nu + Nv - 2];
    }

    for (int ku=1 ; ku<Nu-1 ; ++ku) {
        ret[ku*Nu] = ret[ku*Nu + 1];
        ret[ku*Nu + Nv-1] = ret[ku*Nu + Nv - 2];
    }

    ret[0*Nu + 0] = ret[0*Nu + 1];
    ret[0*Nu + Nv-1] = ret[0*Nu + Nv - 1];
    ret[(Nu-1)*Nu + 0] = ret[(Nu-1)*Nu + 1];
    ret[(Nu-1)*Nu + Nv-1] = ret[(Nu-1)*Nu + Nv - 1];

    return ret;
}
























