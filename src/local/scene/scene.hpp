
/** TP 4ETI - CPE Lyon - 2013/2014 */

#pragma once

#ifndef SCENE_HPP
#define SCENE_HPP

#include <GL/gl.h>
#include <GL/glew.h>

#include "../../lib/3d/mat3.hpp"
#include "../../lib/3d/vec3.hpp"
#include "../../lib/mesh/mesh_parametric.hpp"
#include "../../lib/opengl/mesh_opengl.hpp"
#include "../../lib/interface/camera_matrices.hpp"

#include <vector>

class myWidgetGL;

class scene
{
public:

    scene();

    void reset();

    cpe::vec3 colormap_cool_warm(double k);


    


    /** \brief Method called only once at the beginning (load off files ...) */
    void load_scene(int curv, int surf);

    /** \brief Method called at every frame */
    void draw_scene();

    /** Set the pointer to the parent Widget */
    void set_widget(myWidgetGL* widget_param);

    /** Load basic data for the scene */
    void load_common_data();


private:

    int c;
    int s;

    void build_surface();

    void create_surface_Cylinder (int Nu, int Nv);
    void create_surface_Sphere (int Nu, int Nv);
    void create_surface_Hyperbolic_Paraboloid (int Nu, int Nv);
    void create_surface_Catenoid (int Nu, int Nv);
    void create_surface_Right_Helicoid (int Nu, int Nv);
    void create_surface_Pseudo_Sphere (int Nu, int Nv);
    void create_surface_Sinusoid (int Nu, int Nv);
    void create_surface_Gaussian(int Nu, int Nv);


    cpe::vec3 *derivate_u(int Nu, int Nv, double du);
    cpe::vec3 *derivate_v(int Nu, int Nv, double dv);
    cpe::vec3 *derivate2_u(int Nu, int Nv, double du);
    cpe::vec3 *derivate2_v(int Nu, int Nv, double dv);
    cpe::vec3 *derivate2_uv(int Nu, int Nv, double du, double dv);

    cpe::vec2 principal_curvatures(cpe::vec3 const& Su, cpe::vec3 const& Sv, cpe::vec3 const& Suu, cpe::vec3 const& Svv, cpe::vec3 const& Suv);
    
    void set_color(int Nu, int Nv, double du, double dv);



    /** Load a texture from a given file and returns its id */
    GLuint load_texture_file(std::string const& filename);

    /** Access to the parent object */
    myWidgetGL* pwidget;

    /** Default id for the texture (white texture) */
    GLuint texture_default;

    /** The id of the shader do draw meshes */
    GLuint shader_program_id;


    cpe::mesh_parametric surface;
    cpe::mesh_opengl surface_opengl;


};

#endif
