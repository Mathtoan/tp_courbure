#pragma once

#ifndef MY_WINDOW_HPP
#define MY_WINDOW_HPP


#include <QMainWindow>




//forward declaration
namespace Ui
{
class MainWindow;
}
class myWidgetGL;

/** Declaration of the Window class */
class myWindow: public QMainWindow
{
    Q_OBJECT

public:

    myWindow(QWidget *parent=NULL);
    ~myWindow();



private slots:

    /** Quit the application */
    void action_quit();
    /** Enable the drawing of the meshes */
    void action_draw();
    /** Set the Wireframe mode for the meshes */
    void action_wireframe();


    void action_curvature_gauss_Analytic();
    void action_curvature_mean_Analytic();

    void action_curvature_gauss();
    void action_curvature_mean();
    void action_curvature_principal_1();
    void action_curvature_principal_2();

    void action_surface_Sphere();
    void action_surface_Hyperbolic_Paraboloid();
    void action_surface_Catenoid();
    void action_surface_Right_Helicoid();
    void action_surface_Pseudo_Sphere();
    void action_surface_Sinusoid();
    void action_surface_Gaussian();

private:

    /** Layout for the Window */
    Ui::MainWindow *ui;
    /** The OpenGL Widget */
    myWidgetGL *glWidget;





};

#endif
