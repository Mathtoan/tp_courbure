#include "myWindow.hpp"

#include "myWidgetGL.hpp"
#include "../../lib/common/error_handling.hpp"
#include "ui_mainwindow.h"

#include <iostream>

myWindow::myWindow(QWidget *parent)
    :QMainWindow(parent),ui(new Ui::MainWindow)
{
    try
    {
        //Setup window layout
        ui->setupUi(this);

        //Create openGL context
        QGLFormat qglFormat;
        qglFormat.setVersion(1,2);

        //Create OpenGL Widget renderer
        glWidget=new myWidgetGL(qglFormat);

        //Add the OpenGL Widget into the layout
        ui->layout_scene->addWidget(glWidget);
    }
    catch(cpe::exception_cpe const& e)
    {
        std::cout<<std::endl<<e.report_exception()<<std::endl;
    }


    //Connect slot and signals
    connect(ui->quit,       SIGNAL(clicked()), this, SLOT(action_quit()));
    connect(ui->draw,       SIGNAL(clicked()), this, SLOT(action_draw()));
    connect(ui->wireframe,  SIGNAL(clicked()), this, SLOT(action_wireframe()));

    connect(ui->curvature_gauss_Analytic, SIGNAL(clicked()), this, SLOT(action_curvature_gauss_Analytic()));
    connect(ui->curvature_mean_Analytic,  SIGNAL(clicked()), this, SLOT(action_curvature_mean_Analytic()));
    connect(ui->curvature_gauss,          SIGNAL(clicked()), this, SLOT(action_curvature_gauss()));
    connect(ui->curvature_mean,           SIGNAL(clicked()), this, SLOT(action_curvature_mean()));
    connect(ui->curvature_principal_1,    SIGNAL(clicked()), this, SLOT(action_curvature_principal_1()));
    connect(ui->curvature_principal_2,    SIGNAL(clicked()), this, SLOT(action_curvature_principal_2()));

    connect(ui->surface_Sphere,                 SIGNAL(clicked()), this, SLOT(action_surface_Sphere()));
    connect(ui->surface_Hyperbolic_Paraboloid,  SIGNAL(clicked()), this, SLOT(action_surface_Hyperbolic_Paraboloid()));
    connect(ui->surface_Catenoid,               SIGNAL(clicked()), this, SLOT(action_surface_Catenoid()));
    connect(ui->surface_Right_Helicoid,         SIGNAL(clicked()), this, SLOT(action_surface_Right_Helicoid()));
    connect(ui->surface_Pseudo_Sphere,          SIGNAL(clicked()), this, SLOT(action_surface_Pseudo_Sphere()));
    connect(ui->surface_Sinusoid,               SIGNAL(clicked()), this, SLOT(action_surface_Sinusoid()));
    connect(ui->surface_Gaussian,               SIGNAL(clicked()), this, SLOT(action_surface_Gaussian()));

}



myWindow::~myWindow()
{}

void myWindow::action_quit() {
    close();
}

void myWindow::action_draw() {
    glWidget->change_draw_state();
}

void myWindow::action_wireframe() {
    bool const state_wireframe=ui->wireframe->isChecked();
    glWidget->wireframe(state_wireframe);
}




void myWindow::action_curvature_gauss_Analytic() {
    glWidget->load_curvature_gauss_Analytic();
}

void myWindow::action_curvature_mean_Analytic() {
    glWidget->load_curvature_mean_Analytic();
}


void myWindow::action_curvature_gauss() {
    glWidget->load_curvature_gauss();
}

void myWindow::action_curvature_mean() {
    glWidget->load_curvature_mean();
}

void myWindow::action_curvature_principal_1() {
    glWidget->load_curvature_principal_1();
}

void myWindow::action_curvature_principal_2() {
    glWidget->load_curvature_principal_2();
}



void myWindow::action_surface_Sphere() {
    ui->curvature_gauss_Analytic->setEnabled(true);
    ui->curvature_mean_Analytic->setEnabled(true);
    glWidget->load_surface_Sphere();
}

void myWindow::action_surface_Hyperbolic_Paraboloid() {
    ui->curvature_gauss_Analytic->setEnabled(true);
    ui->curvature_mean_Analytic->setEnabled(true);
    glWidget->load_surface_Hyperbolic_Paraboloid();
}

void myWindow::action_surface_Catenoid() {
    ui->curvature_gauss_Analytic->setEnabled(true);
    ui->curvature_mean_Analytic->setEnabled(true);
    glWidget->load_surface_Catenoid();
}

void myWindow::action_surface_Right_Helicoid() {
    ui->curvature_gauss_Analytic->setEnabled(true);
    ui->curvature_mean_Analytic->setEnabled(true);
    glWidget->load_surface_Right_Helicoid();
}

void myWindow::action_surface_Pseudo_Sphere() {
    ui->curvature_gauss_Analytic->setEnabled(true);
    ui->curvature_mean_Analytic->setEnabled(true);
    glWidget->load_surface_Pseudo_Sphere();
}

void myWindow::action_surface_Sinusoid() {
    if (ui->curvature_gauss_Analytic->isChecked() || ui->curvature_mean_Analytic->isChecked()) {
        ui->curvature_gauss->setChecked(true);
    }

    ui->curvature_gauss_Analytic->setEnabled(false);
    ui->curvature_mean_Analytic->setEnabled(false);

    glWidget->load_surface_Sinusoid();
}

void myWindow::action_surface_Gaussian() {
    if (ui->curvature_gauss_Analytic->isChecked() || ui->curvature_mean_Analytic->isChecked()) {
        ui->curvature_gauss->setChecked(true);
    }

    ui->curvature_gauss_Analytic->setEnabled(false);
    ui->curvature_mean_Analytic->setEnabled(false);
    
    glWidget->load_surface_Gaussian();
}








































